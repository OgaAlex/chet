<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */
  $ability_labels = ['god' => 'Бессмертие', 'fly' => 'Полет', 'idclip' => 'Прохождение сквозь стены', 'fireball' => 'Файрболлы'];
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['con'] = !empty($_COOKIE['con_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['Biography'] = !empty($_COOKIE['Biography_error']);
  $errors['submit'] = !empty($_COOKIE['submit_error']);
  

  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['fio']) {
    // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
    if($_COOKIE['fio_error'] == '1') 
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя.</div>';
	else $messages[]='<div class="error"> Недопустимые символы в имени.</div>';
  }
  // TODO: тут выдать сообщения об ошибках в других полях.
if ($errors['email']) {
    setcookie('email_error', '', 100000);
    if($_COOKIE['email_error'] == '1') 
	{
      $messages[] = '<div class="error"> Пустое поле в почте</div>';
    } 
	else 
	{
      $messages[] = '<div class="error"> Неверные символы в почте</div>';
    }
  }


if ($errors['year']) {
    setcookie('year_error', '', 100000);
    $messages[] = '<div class="error"> Неверная дата </div>';
  }


if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
    $messages[] = '<div class="error"> Не выбран пол </div>';
  }


if ($errors['abilities']) {
    setcookie('abilities_error', '', 100000);
    $messages[] = '<div class="error"> Не выбрана способность </div>';
  }


if ($errors['con']) {
    setcookie('con_error', '', 100000);
    $messages[] = '<div class="error"> Не выбрано количество конечностей </div>';
}

if ($errors['Biography']) {
    setcookie('Biography_error', '', 100000);
    $messages[] = '<div class="error"> Пустая биография </div>';
}

if ($errors['submit']) {
    setcookie('submit_error', '', 100000);
    $messages[] = '<div class="error"> Вы не ознакомились( </div>';
}





  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  if (isset($_COOKIE['fio_value']))
  $values['fio'] = empty($_COOKIE['fio_value']) || !preg_match('/^[а-яА-Я ]+$/u', $_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  else $values['fio'] ='';
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
    $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
    $values['con'] = empty($_COOKIE['con_value']) ? '' : $_COOKIE['con_value'];
    $values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : $_COOKIE['abilities_value'];
    $values['Biography'] = empty($_COOKIE['Biography_value']) ? '' : $_COOKIE['Biography_value'];
    $values['submit'] = empty($_COOKIE['submit_value']) ? '' : $_COOKIE['submit_value'];

  // TODO: аналогично все поля.

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    $errors = FALSE;
	
    if (empty($_POST['fio'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else if(!preg_match('/^[а-яА-Я ]+$/u', $_POST['fio'])){
        setcookie('fio_error', '2', time() + 30 * 24 * 60 *60);
        $errors=TRUE;
    }
    else {
        setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
    }
    
    		
	if (empty($_POST['email']) || filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === FALSE ) {
		setcookie('email_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	
	else {
		setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['year'])) {
		setcookie('year_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('year_value', $_POST['date'], time() + 365 * 24 * 60 * 60);
	}

	if ( empty($_POST['sex'])) {
		setcookie('sex_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
	}
	

	if (empty($_POST['con'])) {
		setcookie('con_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('con_value', $_POST['con'], time() + 365 * 24 * 60 * 60);
	}
$ability_data = array_keys($ability_labels);
$ability_insert = [];
$abilities = $_POST['abilities'];
  foreach ($ability_data as $ability)
  
{ $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;}
	if (empty($_POST['Biography'])) {
		setcookie('Biography_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('Biography_value', $_POST['bio'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['submit'])) {
		setcookie('submit_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		$check = $_POST['submit'] ? '1' : '0';
		setcookie('submit_value', $check, time() + 365 * 24 * 60 * 60);
	}
	$submit = $_POST['submit'] ? '1' : '0';
// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
        setcookie('email_error', '', 100000);
        setcookie('year_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('con_error', '', 100000);
        setcookie('abilities_error', '', 100000);
        setcookie('Biography_error', '', 100000);
        setcookie('submit_error', '', 100000);

  }
  

  // Сохранение в базу данных.

$user = 'u20370';
$pass = '2394375';
$db = new PDO('mysql:host=localhost;dbname=u20370', $user, $pass,
  array(PDO::ATTR_PERSISTENT => true));

// Подготовленный запрос. Не именованные метки.
try {
  $stmt = $db->prepare("INSERT INTO application SET name = ?, email= ?, year = ?, sex=?, con=?, ability_god = ?, ability_fly = ?, ability_idclip = ?, ability_fireball = ?, Biography = ?");
  $stmt->execute([$_POST['fio'], $_POST['email'], intval($year), $_POST['sex'], $_POST['con'], $ability_insert['god'], $ability_insert['fly'], $ability_insert['idclip'], $ability_insert['fireball'], $_POST['Biography']]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}


  // Сохранение в XML-документ.
  // ...

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
